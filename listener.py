"""Bitbucket webhook listener."""
from os import environ
from flask import Flask, request

app = Flask(__name__)


def display_html(request):
    """
    Helper method to display message in HTML format.

    :param request: HTTP request from flask
    :type  request: werkzeug.local.LocalProxy
    :returns message in HTML format
    :rtype basestring
    """
    url_root = request.url_root
    return "".join([
        """Webhook server online! """,
        """Go to <a href="https://bitbucket.org">Bitbucket</a>""",
        """ to configure your repository webhook for """,
        """<a href="%swebhook">%swebhook</a>""" % (url_root, url_root)
    ])


@app.route("/", methods=["GET"])
def index():
    """Endpoint for the root of the Flask app."""
    return display_html(request)


@app.route("/webhook", methods=["GET", "POST"])
def tracking():
    """Endpoint for receiving webhook from bitbucket."""
    if request.method == "POST":
        data = request.get_json()
        commit_author = data["actor"]["username"]
        commit_hash = data["push"]["changes"][0]["new"]["target"]["hash"][:7]
        commit_url = data["push"]["changes"][0]["new"]["target"]["links"]
        commit_url = commit_url["html"]["href"]
        # notify
        print("Webhook received! %s committed %s" % (commit_author,
                                                     commit_hash))
        return "OK"
    else:
        return display_html(request)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
